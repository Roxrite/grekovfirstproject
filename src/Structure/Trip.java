package Structure;

import java.io.Serializable;

public class Trip implements Serializable {
    private Place arrival;
    private Place departure;
    private String cost;

    public Trip(Place arrival, Place departure, String cost) {
        this.arrival = arrival;
        this.departure = departure;
        this.cost = cost;
    }

    public Place getArrival() {
        return arrival;
    }

    public void setArrival(Place arrival) {
        this.arrival = arrival;
    }

    public Place getDeparture() {
        return departure;
    }

    public void setDeparture(Place departure) {
        this.departure = departure;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
