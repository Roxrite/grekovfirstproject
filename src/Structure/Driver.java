package Structure;

import java.io.Serializable;


public class Driver extends Person implements Serializable {

    private int drivingexperience;
    private String numberofrights;

    public Driver(String name, String surname, String birthdate, String telephonenumber, int drivingexperience, String numberofrights) {
        super(name, surname, birthdate, telephonenumber);
        this.drivingexperience = drivingexperience;
        this.numberofrights = numberofrights;
    }

    public int getDrivingexperience() {
        return drivingexperience;
    }

    public void setDrivingexperience(int drivingexperience) {
        this.drivingexperience = drivingexperience;
    }

    public String getNumberofrights() {
        return numberofrights;
    }

    public void setNumberofrights(String numberofrights) {
        this.numberofrights = numberofrights;
    }

    @Override
    public void displayInfo() {
        System.out.println("Name: " + super.getName() + "; Surname: " + super.getSurname() + "; Date of Birth: " + super.getBirthdate() + "; Phone number: " + super.getTelephonenumber() + "; Driving experience: " + getDrivingexperience() + "; Driver license number: " + getNumberofrights() + ";");
    }
}
