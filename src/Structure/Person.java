package Structure;

import java.io.Serializable;

public abstract class Person implements Serializable {
    private String name;
    private String surname;
    private String birthdate;
    private String telephonenumber;


    Person(String name, String surname, String birthdate, String telephonenumber) {
        this.name = name;
        this.surname = surname;
        this.birthdate = birthdate;
        this.telephonenumber = telephonenumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getTelephonenumber() {
        return telephonenumber;
    }

    public void setTelephonenumber(String telephonenumber) {
        this.telephonenumber = telephonenumber;
    }

    public abstract void displayInfo();
}
