package Structure;

import java.io.Serializable;

public class Place implements Serializable {
    private String station;
    private String city;
    private String country;
    private String dayofweek;
    private String date;
    private String time;

    public Place(String station, String city, String country, String dayofweek, String date, String time) {
        this.station = station;
        this.city = city;
        this.country = country;
        this.dayofweek = dayofweek;
        this.date = date;
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDayofweek() {
        return dayofweek;
    }

    public void setDayofweek(String dayofweek) {
        this.dayofweek = dayofweek;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
