package Structure;

import java.io.Serializable;

public class Client extends Person implements Serializable {
    private String email;
    private String login;
    private String password;

    public Client(String name, String surname, String birthdate, String telephonenumber, String email, String login, String password) {
        super(name, surname, birthdate, telephonenumber);
        this.email = email;
        this.login = login;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void displayInfo() {
        System.out.println("Name: " + super.getName() + "; Surname: " + super.getSurname() + "; Date of Birth: " + super.getBirthdate() + "; Phone number: " + super.getTelephonenumber() + "; User Email: " + getEmail() + "; User Login: " + getLogin() + "; User Password: " + getPassword() + ";");
    }
}
