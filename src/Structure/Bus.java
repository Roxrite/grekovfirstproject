package Structure;

import java.io.Serializable;

public class Bus implements Serializable {
    private String make;
    private String model;
    private int year;
    private int numberofseats;
    private String licenseplate;

    public Bus(String make, String model, int year, int numberofseats, String licenseplate) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.numberofseats = numberofseats;
        this.licenseplate = licenseplate;
    }

    public String getLicenseplate() {
        return licenseplate;
    }

    public void setLicenseplate(String licenseplate) {
        this.licenseplate = licenseplate;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getNumberofseats() {
        return numberofseats;
    }

    public void setNumberofseats(int numberofseats) {
        this.numberofseats = numberofseats;
    }
}
