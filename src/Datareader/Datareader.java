package Datareader;

import Structure.*;
import Validation.Validation;

import java.util.Scanner;

public class Datareader {
    public static Datareader instance;

    private Datareader() {
    }

    public static Datareader getInstance() {
        if (instance == null) {
            instance = new Datareader();
        }
        return instance;
    }
    private Scanner scanner = new Scanner(System.in);

    public Client addClient() {
        Client client = new Client("", "", "", "", "", "", "");
        String name, surname, email, login, password, birthdate, telephonenumber;
        Validation validation;
        System.out.println("Enter Client's name (for example: Tom):");
        do {
            name = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(name));
        client.setName(name);
        System.out.println("Enter Client's surname (for example: Hardy):");
        do {
            surname = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(surname));
        client.setSurname(surname);
        System.out.println("Enter Client's birthdate (for example: 20.05.1998):");
        do {
            birthdate = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateDate(birthdate));
        client.setBirthdate(birthdate);
        System.out.println("Enter Client's email (for example: ABCabcdef@mail.ru):");
        do {
            email = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateEmail(email));
        client.setEmail(email);
        System.out.println("Enter Client's login (for example: Albert18):");
        do {
            login = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateLogin(login));
        client.setLogin(login);
        System.out.println("Enter Client's password (for example: abcABC123):");
        do {
            password = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validatePassword(password));
        client.setPassword(password);
        System.out.println("Enter Client's telephonenumber (for example: 80295558844):");
        do {
            telephonenumber = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateTelephonenumber(telephonenumber));
        client.setTelephonenumber(telephonenumber);
        return client;
    }

    public Bus addBus() {
        Bus bus = new Bus("", "", 0, 0, "");
        String make, model, licenseplate;
        int year, numberofseats;
        Validation validation;
        System.out.println("Enter make of bus (for example: Ford):");
        do {
            make = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(make));
        bus.setMake(make);
        System.out.println("Enter model of bus (for example: Transit):");
        do {
            model = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(model));
        bus.setModel(model);
        System.out.println("Enter year of bus (for example: 1999):");
        do {
            year = scanner.nextInt();
            validation = new Validation();
        } while (!validation.validateYear(year));
        bus.setYear(year);
        System.out.println("Enter number of seats of bus (for example: 50):");
        do {
            numberofseats = scanner.nextInt();
            validation = new Validation();
        } while (!validation.validateDrivingexperienceNumberofseats(numberofseats));
        bus.setNumberofseats(numberofseats);
        System.out.println("Enter license plate of bus (for example: 1234AB-7):");
        do {
            licenseplate = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateLicensePlate(licenseplate));
        bus.setLicenseplate(licenseplate);
        return bus;
    }

    public Driver addDriver() {
        Driver driver = new Driver("", "", "", "", 0, "");
        String name, surname, birthdate, numberofrights, telephonenumber;
        int drivingexperience;
        Validation validation;
        System.out.println("Enter Driver's name (for example: Tom):");
        do {
            name = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(name));
        driver.setName(name);
        System.out.println("Enter Driver's surname (for example: Hardy):");
        do {
            surname = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(surname));
        driver.setSurname(surname);
        System.out.println("Enter Driver's birthdate (for example: 20.05.1998):");
        do {
            birthdate = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateDate(birthdate));
        driver.setBirthdate(birthdate);
        System.out.println("Enter Driver's telephonenumber (for example: 80295558844):");
        do {
            telephonenumber = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateTelephonenumber(telephonenumber));
        driver.setTelephonenumber(telephonenumber);
        System.out.println("Enter Driver's driving experience (for example: 20):");
        do {
            drivingexperience = scanner.nextInt();
            validation = new Validation();
        } while (!validation.validateDrivingexperienceNumberofseats(drivingexperience));
        driver.setDrivingexperience(drivingexperience);
        System.out.println("Enter Driver's series and number of rights (for example: GH3422111):");
        do {
            numberofrights = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNumberofrights(numberofrights));
        driver.setNumberofrights(numberofrights);
        return driver;
    }


    public Trip addTrip() {
        Trip trip = new Trip(new Place("", "", "", "", "", ""), new Place("", "", "", "", "", ""), "");
        String station, city, country, date, dayofweek, time, cost;
        Validation validation;
        System.out.println("Enter Departure Bus Station (for example: Central):");
        do {
            station = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(station));
        trip.getDeparture().setStation(station);
        System.out.println("Enter Departure City (for example: Minsk):");
        do {
            city = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(city));
        trip.getDeparture().setCity(city);
        System.out.println("Enter Departure Country (for example: Belarus):");
        do {
            country = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(country));
        trip.getDeparture().setCountry(country);
        System.out.println("Enter Departure Day of week (for example: Monday):");
        do {
            dayofweek = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateDayofweek(dayofweek));
        trip.getDeparture().setDayofweek(dayofweek);
        System.out.println("Enter Departure Date (for example: 11.11.2011):");
        do {
            date = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateDate(date));
        trip.getDeparture().setDate(date);
        System.out.println("Enter Departure Time (for example: 00:00):");
        do {
            time = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateTime(time));
        trip.getDeparture().setTime(time);
        System.out.println("Enter Arrival Bus Station (for example: Central):");
        do {
            station = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(station));
        trip.getArrival().setStation(station);
        System.out.println("Enter Arrival City (for example: Minsk):");
        do {
            city = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(city));
        trip.getArrival().setCity(city);
        System.out.println("Enter Arrival Country (for example: Belarus):");
        do {
            country = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(country));
        trip.getArrival().setCountry(country);
        System.out.println("Enter Arrival Day of week (for example: Monday):");
        do {
            dayofweek = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateDayofweek(dayofweek));
        trip.getArrival().setDayofweek(dayofweek);

        System.out.println("Enter Arrival Date (for example: 11.11.2011):");
        do {
            date = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateDate(date));
        trip.getArrival().setDate(date);
        System.out.println("Enter Arrival Time (for example: 00:00):");
        do {
            time = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateTime(time));
        trip.getArrival().setTime(time);
        System.out.println("Enter Cost of Trip in BYN (for example: 40.00):");
        do {
            cost = scanner.nextLine();
            validation = new Validation();
        } while (!validation.validateCost(cost));
        trip.setCost(cost);
        return trip;
    }
}
