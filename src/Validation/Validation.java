package Validation;

import Structure.DayOfWeek;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    public boolean validateNameSurnameMakeModelStationCityCountry(String validstring) {
        Pattern s = Pattern.compile("^[A-Z][a-z-_.]{1,20}$");
        Matcher stringmatch = s.matcher(validstring);
        boolean stringfound = stringmatch.matches();
        if (stringfound) {
            return stringfound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateTelephonenumber(String validtel) {
        Pattern tel = Pattern.compile("(80?(29|25|44|33))\\d{7}");
        Matcher telmatch = tel.matcher(validtel);
        boolean telfound = telmatch.matches();
        if (telfound) {
            return telfound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateEmail(String validemail) {
        Pattern email = Pattern.compile("([A-Za-z0-9_]+)(@mail|@gmail|@yandex).(ru|com)");
        Matcher emailmatch = email.matcher(validemail);
        boolean emailfound = emailmatch.matches();
        if (emailfound) {
            return emailfound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateLogin(String validlogin) {
        Pattern login = Pattern.compile("[A-Za-z0-9_]+");
//        Pattern login = Pattern.compile("[A-Za-z0-9_]+");
        Matcher loginmatch = login.matcher(validlogin);
        boolean loginfound = loginmatch.matches();
        if (loginfound) {
            return loginfound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validatePassword(String validpassword) {
        Pattern password = Pattern.compile("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9@#$%]).{8,}");
        Matcher passwordmatch = password.matcher(validpassword);
        boolean passwordfound = passwordmatch.matches();
        if (passwordfound) {
            return passwordfound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateDrivingexperienceNumberofseats(int validint) {
        Pattern i = Pattern.compile("[0-9]|([0-9]{2})");
        Matcher imatch = i.matcher(String.valueOf(validint));
        boolean ifound = imatch.matches();
        if (ifound) {
            return ifound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateNumberofrights(String validnumrights) {
        Pattern numrights = Pattern.compile("([A-Z0-9]{3})([0-9]{6})");
        Matcher numrightsmatch = numrights.matcher(validnumrights);
        boolean numrightsfound = numrightsmatch.matches();
        if (numrightsfound) {
            return numrightsfound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateYear(int validyear) {
        Pattern year = Pattern.compile("^(19|20)\\d\\d$");
        Matcher yearmatch = year.matcher(String.valueOf(validyear));
        boolean yearfound = yearmatch.matches();
        if (yearfound) {
            return yearfound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateCost(String validcost) {
        Pattern cost = Pattern.compile("^(([0-9])|([0-9]{2})).([0-9]{2})$");
        Matcher costmatch = cost.matcher(validcost);
        boolean costfound = costmatch.matches();
        if (costfound) {
            return costfound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateLicensePlate(String validlicenseplate) {
        Pattern cost = Pattern.compile("^(([0-9]{4})([A-Z]{2})-[0-9])$");
        Matcher licenseplatematch = cost.matcher(validlicenseplate);
        boolean licenseplatefound = licenseplatematch.matches();
        if (licenseplatefound) {
            return licenseplatefound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateDayofweek(String validdayofweek) {
        boolean dayofweekfound = true;
        try {
            DayOfWeek.valueOf(validdayofweek);
        } catch (IllegalArgumentException e) {
            System.out.println("Data entered incorrectly. Please try again.");
            dayofweekfound = false;
        }
        return dayofweekfound;
    }

    public boolean validateDate(String validdate) {
        Pattern date = Pattern.compile("(0[1-9]|[12][0-9]|3[01]).(0[1-9]|1[012]).(19|20)([0-9]{2})");
        Matcher datematch = date.matcher(validdate);
        boolean datefound = datematch.matches();
        if (datefound) {
            return datefound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }

    public boolean validateTime(String validtime) {
        Pattern time = Pattern.compile("([01][0-9]|2[0-3]):[0-5][0-9]");
        Matcher timematch = time.matcher(validtime);
        boolean timefound = timematch.matches();
        if (timefound) {
            return timefound;
        } else {
            System.out.println("Data entered incorrectly. Please try again.");
            return false;
        }
    }
}
