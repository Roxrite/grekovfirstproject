package StateSaver;

import java.io.*;
import java.util.ArrayList;

public class StateSaver<A> implements Serializable{

    private String filename;
    public ArrayList<A> list = new ArrayList<>();

    public StateSaver(String filename) {
        this.filename = filename;
    }

    public void saveState() {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream("file" + filename));
            out.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<A> restoreState() {
        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(new FileInputStream("file" + filename));
            try {
                list = (ArrayList<A>) in.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
