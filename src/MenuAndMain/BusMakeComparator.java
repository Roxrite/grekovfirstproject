package MenuAndMain;

import Structure.Bus;

import java.util.Comparator;

public class BusMakeComparator implements Comparator<Bus> {
    public int compare(Bus a, Bus b) {
        return a.getMake().compareTo(b.getMake());
    }
}
