package MenuAndMain;

import Datareader.Datareader;

public class Main {

    private Menu.IMenuController controller = new Menu.IMenuController() {

        @Override
        public void addClient() {
            Datareader.getInstance().addClient();
        }

        @Override
        public void addDriver() {
            Datareader.getInstance().addDriver();
        }

        @Override
        public void addBus() {
            Datareader.getInstance().addBus();
        }

        @Override
        public void addTrip() {
            Datareader.getInstance().addTrip();
        }
    };

    Menu menu = new Menu(controller);

    public static void main(String[] args) {
        new Main();
        Main main = new Main();
    }

    Main() {
        menu.startMainMenu();
    }
}
