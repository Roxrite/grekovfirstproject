package MenuAndMain;

import Structure.Driver;

import java.util.Comparator;

public class DriverSurnameComparator implements Comparator<Driver> {
    public int compare(Driver a, Driver b) {
        return a.getSurname().compareTo(b.getSurname());
    }
}
