package MenuAndMain;

import Datareader.Datareader;
import StateSaver.StateSaver;
import Structure.Bus;
import Structure.Client;
import Structure.Driver;
import Structure.Trip;
import Validation.Validation;

import java.util.*;

class Menu {
    public interface IMenuController {

        void addClient();

        void addDriver();

        void addBus();

        void addTrip();
    }

    private StateSaver<Driver> driverStateSaver = new StateSaver<Driver>("Driver");
    private StateSaver<Client> clientStateSaver = new StateSaver<Client>("Client");
    private StateSaver<Bus> busStateSaver = new StateSaver<Bus>("Bus");
    private StateSaver<Trip> tripStateSaver = new StateSaver<Trip>("Trip");
    private IMenuController controller;
    private Scanner scanner = new Scanner(System.in);

    Menu(IMenuController controller) {
        this.controller = controller;
    }

    public class SaverThread extends Thread {

        @Override
        public void run() {
            System.out.println("Saving in progress. Please wait.");
            try {
                driverStateSaver.saveState();
                clientStateSaver.saveState();
                busStateSaver.saveState();
                tripStateSaver.saveState();
                Thread.sleep(5000);
                System.out.println("Data saved successfully.\n");
            } catch (InterruptedException e) {
                System.out.println("Saving is interrupted. Please try again.");
            }
        }
    }

    void startMainMenu() {
        System.out.println("Welcome to the company TravelBus");
        System.out.println("Please, choose the necessary option:");
        System.out.println("1 - Log in as Admin");
        System.out.println("2 - Variety of trips");
        System.out.println("3 - Sales");
        System.out.println("4 - Contact information");
        System.out.println("0 - Exit");
        int result = 0;
        boolean confirmation = true;
        while (confirmation) {
            if (scanner.hasNextInt()) {
                result = scanner.nextInt();
                if (result >= 0 && result <= 4) {
                    confirmation = false;
                } else {
                    System.out.println("Incorrect entry. Please, try again.");
                }
            } else {
                scanner.nextLine();
                System.out.println("Incorrect entry. Please, try again.");
            }
        }
        switch (result) {
            case 1:
                System.out.println("If you enter the wrong login or username or password more than 3 times, you will go to the main menu.");
                for (int i = 0; i < 3; i++) {
                    System.out.println("Enter login or username:");
                    String adminlogin = scanner.next();
                    System.out.println("Enter password:");
                    String adminpassword = scanner.next();
                    if (adminlogin.equals("AdminRG") && adminpassword.equals("12345")) {
                        startAdminMenu();
                    } else {
                        System.out.println("Incorrect login or password. Please try again.");
                    }
                }
                startMainMenu();
                break;
            case 2:
                startAllTripsMenu();
                break;
            case 3:
                generateSalesTripsReport();
                break;
            case 4:
                enterToContactInformation();
                break;
            case 0:
                System.out.println("Thank you for the visit. Have a nice day!");
                System.exit(0);
        }
    }


    private void startAdminMenu() {
        System.out.println("Welcome, Rostislav");
        System.out.println("Please, choose the necessary option:");
        System.out.println("1 - Changing of data");
        System.out.println("2 - Create the report");
        System.out.println("0 - Log out");
        int result = 0;
        boolean confirmation = true;
        while (confirmation) {
            if (scanner.hasNextInt()) {
                result = scanner.nextInt();
                if (result >= 0 && result <= 2) {
                    confirmation = false;
                } else {
                    System.out.println("Incorrect entry. Please, try again.");
                }
            } else {
                scanner.nextLine();
                System.out.println("Incorrect entry. Please, try again.");
            }
        }
        switch (result) {
            case 1:
                editdataAdminMenu();
                break;
            case 2:
                generateReportAdminMenu();
            case 0:
                saveBeforeGoingToMainMenu();
        }

    }

    private void editdataAdminMenu() {
        System.out.println("Please, choose the necessary option:");
        System.out.println("1 - Add new client");
        System.out.println("2 - Add new driver");
        System.out.println("3 - Add new bus");
        System.out.println("4 - Add new trip");
        System.out.println("5 - Remove client");
        System.out.println("6 - Remove driver");
        System.out.println("7 - Remove bus");
        System.out.println("8 - Remove trip");
        System.out.println("0 - Exit to Admin Menu");
        try {
            int result = 0;
            boolean confirmation = true;
            while (confirmation) {
                if (scanner.hasNextInt()) {
                    result = scanner.nextInt();
                    if (result >= 0 && result <= 8) {
                        confirmation = false;
                    } else {
                        System.out.println("Incorrect entry. Please, try again.");
                    }
                } else {
                    scanner.nextLine();
                    System.out.println("Incorrect entry. Please, try again.");
                }
            }
            switch (result) {
                case 1:
                    clientStateSaver.list.add(Datareader.getInstance().addClient());
                    Thread.sleep(2000);
                    System.out.println("\nClient is successfully added");
                    goBackToAdminMenuWithSaving();
                    break;
                case 2:
                    driverStateSaver.list.add(Datareader.getInstance().addDriver());
                    Thread.sleep(2000);
                    System.out.println("\nDriver is successfully added");
                    goBackToAdminMenuWithSaving();
                    break;
                case 3:
                    busStateSaver.list.add(Datareader.getInstance().addBus());
                    Thread.sleep(2000);
                    System.out.println("\nBus is successfully added");
                    goBackToAdminMenuWithSaving();
                    break;
                case 4:
                    tripStateSaver.list.add(Datareader.getInstance().addTrip());
                    Thread.sleep(2000);
                    System.out.println("\nTrip is successfully added");
                    goBackToAdminMenuWithSaving();
                    break;
                case 5:
                    clientStateSaver.restoreState();
                    removeClient();
                    break;
                case 6:
                    driverStateSaver.restoreState();
                    removeDriver();
                    break;
                case 7:
                    busStateSaver.restoreState();
                    removeBus();
                    break;
                case 8:
                    tripStateSaver.restoreState();
                    removeTrip();
                    break;
                case 0:
                    Thread.sleep(1000);
                    startAdminMenu();
                    break;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void generateReportAdminMenu() {
        System.out.println("Please, choose the necessary option:");
        System.out.println("1 - Trip report");
        System.out.println("2 - Client report");
        System.out.println("3 - Driver report");
        System.out.println("4 - Bus report");
        System.out.println("0 - Exit to Admin Menu");
        int result = 0;
        boolean confirmation = true;
        while (confirmation) {
            if (scanner.hasNextInt()) {
                result = scanner.nextInt();
                if (result >= 0 && result <= 4) {
                    confirmation = false;
                } else {
                    System.out.println("Incorrect entry. Please, try again.");
                }
            } else {
                scanner.nextLine();
                System.out.println("Incorrect entry. Please, try again.");
            }
        }
        switch (result) {
            case 1:
                tripStateSaver.restoreState();
                generateAllTripsReport();
                goBackToAdminMenuWithoutSaving();
                break;
            case 2:
                clientStateSaver.restoreState();
                generateClientReport();
                break;
            case 3:
                driverStateSaver.restoreState();
                generateDriverReport();
                break;
            case 4:
                busStateSaver.restoreState();
                generateBusReport();
                break;
            case 0:
                startAdminMenu();
                break;
        }
    }


    private void saveBeforeGoingToMainMenu() {
        System.out.println("Save changes before going to the Main menu?");
        System.out.println("1 - Yes");
        System.out.println("2 - No");
        int result = 0;
        boolean confirmation = true;
        while (confirmation) {
            if (scanner.hasNextInt()) {
                result = scanner.nextInt();
                if (result == 1 || result == 2) {
                    confirmation = false;
                } else {
                    System.out.println("Incorrect entry. Please, try again.");
                }
            } else {
                scanner.nextLine();
                System.out.println("Incorrect entry. Please, try again.");
            }
        }
        switch (result) {
            case 1:
                SaverThread saver = new SaverThread();
                saver.start();
                try {
                    saver.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startMainMenu();
                break;
            case 2:
                startMainMenu();
                break;
        }
    }

    private void startAllTripsMenu() {
        System.out.println("Please, choose the necessary option:");
        System.out.println("1 - List of trips");
        System.out.println("2 - Search");
        System.out.println("0 - Exit to the Main Menu");
        int result = 0;
        boolean confirmation = true;
        while (confirmation) {
            if (scanner.hasNextInt()) {
                result = scanner.nextInt();
                if (result >= 0 && result <= 2) {
                    confirmation = false;
                } else {
                    System.out.println("Incorrect entry. Please, try again.");
                }
            } else {
                scanner.nextLine();
                System.out.println("Incorrect entry. Please, try again.");
            }
        }
        switch (result) {
            case 1:
                tripStateSaver.restoreState();
                generateAllTripsReport();
                System.out.println("To place an order, contact us at (+375 17) 222-22-22 or register on our website.\n");
                System.out.println("Before exit to the All Trips Menu, please, click 0");
                do {
                    result = scanner.nextInt();
                } while (result != 0);
                startAllTripsMenu();
                break;
            case 2:
                tripStateSaver.restoreState();
                searchTrip();
                break;
            case 0:
                startMainMenu();
        }
    }

    private void enterToContactInformation() {
        System.out.println("(+375 17) 222-22-22 - tourist call center\n" +
                "www.232323.by - web call center (Online communication with the agency manager and selection of tours)\n" +
                "Ltd \"Company TravelBus\"\n" +
                "Address: 333333, Minsk, Tris Street, 18, office 111\n" +
                "Tel / Fax (Agency only):\n" +
                "(+375 17) 555-55-55 \n" +
                "(+375 17) 666-66-66 fax (in auto mode) \n" +
                "E-mail: info@minsk.travel-bus.com");
        System.out.println("Before exit to the Main Menu, please, click 0");
        int result;
        do {
            result = scanner.nextInt();
        } while (result != 0);
        startMainMenu();
    }

    private void searchTrip() {
        int result;
        String depstation, depcity, depdate, deptime, arstation, arcity, cost;
        Validation validation;
        System.out.println("Enter Departure Bus station (for example: Central):");
        do {
            depstation = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(depstation));
        System.out.println("Enter Departure City (for example: Minsk):");
        do {
            depcity = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(depcity));
        System.out.println("Enter Arrival Bus station (for example: Central):");
        do {
            arstation = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(arstation));
        System.out.println("Enter Arrival City (for example: Minsk):");
        do {
            arcity = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(arcity));
        System.out.println("Enter Departure date (for example: 11.11.2011):");
        do {
            depdate = scanner.next();
            validation = new Validation();
        } while (!validation.validateDate(depdate));
        System.out.println("Enter Departure time (for example: 00:00):");
        do {
            deptime = scanner.next();
            validation = new Validation();
        } while (!validation.validateTime(deptime));
        System.out.println("Enter Cost of Trip in BYN (for example: 40.00):");
        do {
            cost = scanner.next();
            validation = new Validation();
        } while (!validation.validateCost(cost));
        ArrayList<Trip> searchlist = new ArrayList<>();
        for (int i = 0; i < tripStateSaver.list.size(); i++) {
            if (depstation.equals(tripStateSaver.list.get(i).getDeparture().getStation()) && depcity.equals(tripStateSaver.list.get(i).getDeparture().getCity()) &&
                    arstation.equals(tripStateSaver.list.get(i).getArrival().getStation()) && arcity.equals(tripStateSaver.list.get(i).getArrival().getCity()) &&
                    depdate.equals(tripStateSaver.list.get(i).getDeparture().getDate()) && deptime.equals(tripStateSaver.list.get(i).getDeparture().getTime()) && cost.equals(tripStateSaver.list.get(i).getCost())) {
                searchlist.add(tripStateSaver.list.get(i));
            }
        }
        System.out.println("\nSearch...");
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (searchlist.size()==0) {
            System.out.println("\nNo results were found for your request.\n");
            System.out.println("Before exit to the All Trips Menu, please, click 0");
            do {
                result = scanner.nextInt();
            } while (result != 0);
            startAllTripsMenu();
        }
        else {
            System.out.println("\n==============================================================================================================================================================================================================================================");
            System.out.printf("%-3s%-22s%-15s%-18s%-22s%-15s%-15s%-20s%-13s%-16s%-20s%-13s%-13s%-5s%n", "№", "| Departure Bus Station ", "| Departure City ", "| Departure Country ", "| Departure Day of Week ", "| Departure Date ", "| Departure Time ", "| Arrival Bus Station ",
                    "| Arrival City ", "| Arrival Country ", "| Arrival Day of Week ", "| Arrival Date ", "| Arrival Time ", "| Cost ");
            for (int i = 0; i < searchlist.size(); i++) {
                System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                System.out.printf("%-6s%-24s%-17s%-20s%-24s%-17s%-17s%-22s%-15s%-18s%-22s%-15s%-15s%-7s%n", (i + 1), searchlist.get(i).getDeparture().getStation(), searchlist.get(i).getDeparture().getCity(),
                        searchlist.get(i).getDeparture().getCountry(), searchlist.get(i).getDeparture().getDayofweek(), searchlist.get(i).getDeparture().getDate(),
                        searchlist.get(i).getDeparture().getTime(), searchlist.get(i).getArrival().getStation(), searchlist.get(i).getArrival().getCity(),
                        searchlist.get(i).getArrival().getCountry(), searchlist.get(i).getArrival().getDayofweek(), searchlist.get(i).getArrival().getDate(),
                        searchlist.get(i).getArrival().getTime(), searchlist.get(i).getCost());
            }
            System.out.println("==============================================================================================================================================================================================================================================");
            System.out.println("To place an order, contact us at (+375 17) 222-22-22 or register on our website.\n");
            System.out.println("Before exit to the All Trips Menu, please, click 0");
            do {
                result = scanner.nextInt();
            } while (result != 0);
            startAllTripsMenu();
        }
    }



    private void generateAllTripsReport() {
        System.out.println("\n==============================================================================================================================================================================================================================================");
        System.out.printf("%-3s%-22s%-15s%-18s%-22s%-15s%-15s%-20s%-13s%-16s%-20s%-13s%-13s%-5s%n", "№", "| Departure Bus Station ", "| Departure City ", "| Departure Country ", "| Departure Day of Week ", "| Departure Date ", "| Departure Time ", "| Arrival Bus Station ",
                "| Arrival City ", "| Arrival Country ", "| Arrival Day of Week ", "| Arrival Date ", "| Arrival Time ", "| Cost ");
        for (int i = 0; i < tripStateSaver.list.size(); i++) {
            System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            System.out.printf("%-6s%-24s%-17s%-20s%-24s%-17s%-17s%-22s%-15s%-18s%-22s%-15s%-15s%-7s%n", (i + 1), tripStateSaver.list.get(i).getDeparture().getStation(), tripStateSaver.list.get(i).getDeparture().getCity(),
                    tripStateSaver.list.get(i).getDeparture().getCountry(), tripStateSaver.list.get(i).getDeparture().getDayofweek(), tripStateSaver.list.get(i).getDeparture().getDate(),
                    tripStateSaver.list.get(i).getDeparture().getTime(), tripStateSaver.list.get(i).getArrival().getStation(), tripStateSaver.list.get(i).getArrival().getCity(),
                    tripStateSaver.list.get(i).getArrival().getCountry(), tripStateSaver.list.get(i).getArrival().getDayofweek(), tripStateSaver.list.get(i).getArrival().getDate(),
                    tripStateSaver.list.get(i).getArrival().getTime(), tripStateSaver.list.get(i).getCost());
        }
        System.out.println("==============================================================================================================================================================================================================================================");
    }

    private void generateSalesTripsReport() {
        int result;
        tripStateSaver.restoreState();
        System.out.println("\n==============================================================================================================================================================================================================================================");
        System.out.printf("%-3s%-22s%-15s%-18s%-22s%-15s%-15s%-20s%-13s%-16s%-20s%-13s%-13s%-5s%n", "№", "| Departure Bus Station ", "| Departure City ", "| Departure Country ", "| Departure Day of Week ", "| Departure Date ", "| Departure Time ", "| Arrival Bus Station ",
                "| Arrival City ", "| Arrival Country ", "| Arrival Day of Week ", "| Arrival Date ", "| Arrival Time ", "| Cost ");
        for (int i = 0; i < tripStateSaver.list.size(); i++) {
            float floatcost = Float.parseFloat(tripStateSaver.list.get(i).getCost());
            if (floatcost <= 35.00) {
                System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                System.out.printf("%-6s%-24s%-17s%-20s%-24s%-17s%-17s%-22s%-15s%-18s%-22s%-15s%-15s%-7s%n", (i + 1), tripStateSaver.list.get(i).getDeparture().getStation(), tripStateSaver.list.get(i).getDeparture().getCity(),
                        tripStateSaver.list.get(i).getDeparture().getCountry(), tripStateSaver.list.get(i).getDeparture().getDayofweek(), tripStateSaver.list.get(i).getDeparture().getDate(),
                        tripStateSaver.list.get(i).getDeparture().getTime(), tripStateSaver.list.get(i).getArrival().getStation(), tripStateSaver.list.get(i).getArrival().getCity(),
                        tripStateSaver.list.get(i).getArrival().getCountry(), tripStateSaver.list.get(i).getArrival().getDayofweek(), tripStateSaver.list.get(i).getArrival().getDate(),
                        tripStateSaver.list.get(i).getArrival().getTime(), tripStateSaver.list.get(i).getCost());
            }
        }
        System.out.println("==============================================================================================================================================================================================================================================");
        System.out.println("To place an order, contact us at (+375 17) 222-22-22 or register on our website.\n");
        System.out.println("Before exit to the Sales Trips Menu, please, click 0");
        do {
            result = scanner.nextInt();
        } while (result != 0);
        startMainMenu();
    }

    private void removeTrip() throws InterruptedException {
        String city, date, time;
        Validation validation;
        System.out.println("Enter Departure city of trip which you want to delete (for example: Minsk):");
        do {
            city = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(city));
        System.out.println("Enter Departure date of trip which you want to delete (for example: 11.11.2011):");
        do {
            date = scanner.next();
            validation = new Validation();
        } while (!validation.validateDate(date));
        System.out.println("Enter Departure time of trip which you want to delete (for example: 00:00):");
        do {
            time = scanner.next();
            validation = new Validation();
        } while (!validation.validateTime(time));
        Thread.sleep(2000);
        for (int i = 0; i < tripStateSaver.list.size(); i++) {
            if (city.equals(tripStateSaver.list.get(i).getDeparture().getCity()) && date.equals(tripStateSaver.list.get(i).getDeparture().getDate()) && time.equals(tripStateSaver.list.get(i).getDeparture().getTime())) {
                tripStateSaver.list.remove(i);
                System.out.println("\nTrip is successfully deleted\n");
                goBackToAdminMenuWithSaving();
            }
        }
        System.out.println("\nTrip with such data was not found\n");
        goBackToAdminMenuWithoutSaving();
    }

    private void generateBusReport() {
        Comparator<Bus> buscomp = new BusMakeComparator();
        busStateSaver.list.sort(buscomp);
        System.out.println("\n=======================================================================================");
        System.out.printf("%-3s%-20s%-21s%-6s%-17s%-17s%n", "№", "| Make ", "| Model ", "| Year ", "| Number of seats ", "| License plate ");
        for (int i = 0; i < busStateSaver.list.size(); i++) {
            System.out.println("---------------------------------------------------------------------------------------");
            System.out.printf("%-6s%-20s%-21s%-7s%-18s%-19s%n", (i + 1), busStateSaver.list.get(i).getMake(), busStateSaver.list.get(i).getModel(), busStateSaver.list.get(i).getYear(),
                    busStateSaver.list.get(i).getNumberofseats(), busStateSaver.list.get(i).getLicenseplate());
        }
        System.out.println("=======================================================================================");
        goBackToAdminMenuWithoutSaving();
    }

    private void generateClientReport() {
        Comparator<Client> clientcomp = new ClientSurnameComparator();
        clientStateSaver.list.sort(clientcomp);
        System.out.println("\n===============================================================================================================================================================");
        System.out.printf("%-3s%-20s%-23s%-15s%-17s%-25s%-25s%-26s%n", "№", "| Surname ", "| Name ", "| Birthdate ", "| Telephonenumber ", "| Email ", "| Login ", "| Password ");
        for (int i = 0; i < clientStateSaver.list.size(); i++) {
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------");
            System.out.printf("%-6s%-20s%-23s%-15s%-18s%-25s%-25s%-26s%n", (i + 1), clientStateSaver.list.get(i).getSurname(), clientStateSaver.list.get(i).getName(), clientStateSaver.list.get(i).getBirthdate(),
                    clientStateSaver.list.get(i).getTelephonenumber(), clientStateSaver.list.get(i).getEmail(), clientStateSaver.list.get(i).getLogin(), clientStateSaver.list.get(i).getPassword());
        }
        System.out.println("===============================================================================================================================================================");
        goBackToAdminMenuWithoutSaving();
    }

    private void generateDriverReport() {
        Comparator<Driver> drivercomp = new DriverSurnameComparator();
        driverStateSaver.list.sort(drivercomp);
        System.out.println("\n===========================================================================================================================================");
        System.out.printf("%-3s%-20s%-23s%-15s%-17s%-25s%-25s%n", "№", "| Surname ", "| Name ", "| Birthdate ", "| Telephonenumber ", "| Driving experience ", "| Series and Number of rights ");
        for (int i = 0; i < driverStateSaver.list.size(); i++) {
            System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------");
            System.out.printf("%-6s%-20s%-23s%-15s%-18s%-25s%-25s%n", (i + 1), driverStateSaver.list.get(i).getSurname(), driverStateSaver.list.get(i).getName(), driverStateSaver.list.get(i).getBirthdate(),
                    driverStateSaver.list.get(i).getTelephonenumber(), driverStateSaver.list.get(i).getDrivingexperience(), driverStateSaver.list.get(i).getNumberofrights());
        }
        System.out.println("===========================================================================================================================================");
        goBackToAdminMenuWithoutSaving();
    }

    private void removeBus() throws InterruptedException {
        String make, model, licenseplate;
        Validation validation;
        System.out.println("Enter Make of bus which you want to delete (for example: Ford):");
        do {
            make = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(make));
        System.out.println("Enter Model of bus which you want to delete (for example: Transit):");
        do {
            model = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(model));
        System.out.println("Enter License Plate of bus which you want to delete (for example: 1234AB-7):");
        do {
            licenseplate = scanner.next();
            validation = new Validation();
        } while (!validation.validateLicensePlate(licenseplate));
        Thread.sleep(2000);
        for (int i = 0; i < busStateSaver.list.size(); i++) {
            if (make.equals(busStateSaver.list.get(i).getMake()) && model.equals(busStateSaver.list.get(i).getModel()) && licenseplate.equals(busStateSaver.list.get(i).getLicenseplate())) {
                busStateSaver.list.remove(i);
                System.out.println("\nBus is successfully deleted\n");
                goBackToAdminMenuWithSaving();
            }
        }
        System.out.println("\nBus with such data was not found\n");
        goBackToAdminMenuWithoutSaving();
    }

    private void removeDriver() throws InterruptedException {
        String name, surname;
        Validation validation;
        System.out.println("Enter Name of Driver which you want to delete (for example: Tom):");
        do {
            name = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(name));
        System.out.println("Enter Surname of Driver which you want to delete (for example: Hardy):");
        do {
            surname = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(surname));
        Thread.sleep(2000);
        for (int i = 0; i < driverStateSaver.list.size(); i++) {
            if (name.equals(driverStateSaver.list.get(i).getName()) && surname.equals(driverStateSaver.list.get(i).getSurname())) {
                driverStateSaver.list.remove(i);
                System.out.println("\nDriver is successfully deleted\n");
                goBackToAdminMenuWithSaving();
            }
        }
        System.out.println("\nDriver with such data was not found\n");
        goBackToAdminMenuWithoutSaving();
    }

    private void removeClient() throws InterruptedException {
        String name, surname;
        Validation validation;
        System.out.println("Enter Name of Client which you want to delete (for example: Tom):");
        do {
            name = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(name));
        System.out.println("Enter Surname of Client which you want to delete (for example: Hardy):");
        do {
            surname = scanner.next();
            validation = new Validation();
        } while (!validation.validateNameSurnameMakeModelStationCityCountry(surname));
        Thread.sleep(2000);
        for (int i = 0; i < clientStateSaver.list.size(); i++) {
            if (name.equals(clientStateSaver.list.get(i).getName()) && surname.equals(clientStateSaver.list.get(i).getSurname())) {
                clientStateSaver.list.remove(i);
                System.out.println("\nClient is successfully deleted\n");
                goBackToAdminMenuWithSaving();
            }
        }
        System.out.println("\nClient with such data was not found\n");
        goBackToAdminMenuWithoutSaving();
    }


    private void goBackToAdminMenuWithSaving() {
        System.out.println("Before exit to the Admin Menu, please, click 0");
        int result;
        do {
            result = scanner.nextInt();
        } while (result != 0);
        saveBeforeGoingToAdminMenu();
    }

    private void goBackToAdminMenuWithoutSaving() {
        System.out.println("Before exit to the Admin Menu, please, click 0");
        int result;
        do {
            result = scanner.nextInt();
        } while (result != 0);
        startAdminMenu();
    }

    private void saveBeforeGoingToAdminMenu() {
        System.out.println("Save changes before going to the Admin menu?");
        System.out.println("1 - Yes");
        System.out.println("2 - No");
        int result = 0;
        boolean confirmation = true;
        while (confirmation) {
            if (scanner.hasNextInt()) {
                result = scanner.nextInt();
                if (result == 1 || result == 2) {
                    confirmation = false;
                } else {
                    System.out.println("Incorrect entry. Please, try again.");
                }
            } else {
                scanner.nextLine();
                System.out.println("Incorrect entry. Please, try again.");
            }
        }
        switch (result) {
            case 1:
                SaverThread saver = new SaverThread();
                saver.start();
                try {
                    saver.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startAdminMenu();
                break;
            case 2:
                startAdminMenu();
                break;
        }
    }
}