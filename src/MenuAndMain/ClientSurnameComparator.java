package MenuAndMain;

import Structure.Client;

import java.util.Comparator;

public class ClientSurnameComparator implements Comparator<Client> {
    public int compare(Client a, Client b) {
        return a.getSurname().compareTo(b.getSurname());
    }
}
